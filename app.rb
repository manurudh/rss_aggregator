require 'sinatra'
require 'sinatra/flash'
require 'nokogiri'
require 'open-uri'
require 'bcrypt'
require 'sinatra/activerecord'
require './config/environments' #database configuration
require './models/favorite'
require './models/user'
require './models/source'


enable :sessions
set :session_secret, '!Ope10n&'

def parse(feed)
  doc = Nokogiri::XML(open(feed))
  doc.search("item").map do |i|
    item = {}
    item[:link] = i.css('link').text
    item[:title] = i.css('title').text
    item
  end
end

def feed(sources)
  feed_items = []
  sources.map do |source|
    feed_items << parse(source.url)   
  end
  feed_items.flatten
end


get '/' do
  if current_user 
    @sources = Source.where(user_id: current_user.id)
    @items = feed(@sources)
  	@favorites = Favorite.where(user_id: current_user.id).pluck(:title)
  	erb :index
 	else 
 		flash[:error] = "Please sign in."
 		redirect 'sessions/new'
 	end
	
end

post '/sources' do
  if current_user
    begin
      doc = Nokogiri::XML(open(params[:source][:url]))
    rescue StandardError => e
      flash[:error] = "Invalid source. Please try again."
      redirect "/"
    end

    @source = current_user.sources.build(params[:source])
    if @source.save
      flash[:success] = "Source saved!"
      redirect '/'
    else
      flash[:error] = "There was an error. Please try again."
      redirect "/"
    end 
  else
    flash[:error] = "Please sign in."
    redirect 'sessions/new'
  end

end

delete '/sources/:id' do
  if current_user
    Source.find(params[:id]).destroy
    flash[:success] = "Deleted source!"
    redirect '/'
  else
    flash[:error] = "Please sign in."
    redirect 'sessions/new'
  end
end

get '/favorites' do	
	if current_user
		@favorites = Favorite.where(user_id: current_user.id)
		erb :favorites
	else
		flash[:error] = "Please sign in."
 		redirect 'sessions/new'
 	end
end

post '/favorites' do
  if current_user	
		@favorite = current_user.favorites.build(params[:favorite])
		if @favorite.save
			flash[:success] = "Saved to favorites!"
			redirect '/'
		else
			"Sorry, there was an error!"
		end	
  else
    flash[:error] = "Please sign in."
    redirect 'sessions/new'
  end
end

delete '/favorites/:id' do
	if current_user
		Favorite.find(params[:id]).destroy
		flash[:success] = "Deleted from favorites!"
		redirect '/favorites'
	else
		flash[:error] = "Please sign in."
 		redirect 'sessions/new'
 	end
end


# Add user creation and authentication.
get '/users/new' do
	@user = User.new
	erb :new_user
end

post '/users/create' do
	@user = User.new(params[:user])
	if @user.save
    flash[:success] = "Signed up!"
    redirect '/'
  else
    flash[:error] = "Invalid parameters. Try again!"
    redirect '/users/new'
  end
end

get '/sessions/new' do
  erb :login
end

post '/sessions/create' do
  user = User.authenticate(params[:email], params[:password])
  if user
    session[:user_id] = user.id
    flash[:success] = "Logged in!"
    redirect '/'
  else
    flash[:error] = "Invalid parameters. Try again!"
    redirect '/sessions/new'
  end
end

get '/sessions/destroy' do
  session[:user_id] = nil
  flash[:success] = "Logged out!"
  redirect '/' 
end


private
	def current_user
  	@current_user ||= User.find(session[:user_id]) if session[:user_id]
	end



