class Source < ActiveRecord::Base

	belongs_to :user
	validates_presence_of :user_id, :on => :create
	validates :url, presence: true, uniqueness: true

end

