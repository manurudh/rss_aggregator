class Favorite < ActiveRecord::Base

	belongs_to :user
	validates_presence_of :user_id, :on => :create
	validates_presence_of :title
	validates_presence_of :link
end