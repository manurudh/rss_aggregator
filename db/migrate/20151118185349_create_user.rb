class CreateUser < ActiveRecord::Migration
  def change
  	create_table :users do |t|
  		t.string :email
  		t.string :password_hash
  		t.string :password_salt
  	end
  	add_index(:users, :email, unique: true)
  	add_index(:users, :id, unique: true)
  end
end
