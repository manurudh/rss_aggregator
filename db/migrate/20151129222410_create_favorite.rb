class CreateFavorite < ActiveRecord::Migration
  def change
  	create_table :favorites do |t|
  		t.string :title
  		t.string :link
  	end
  	add_reference(:favorites, :user, index: true, foreign_key: true)
  	add_index(:favorites, [:title, :link], unique: true)
  end
end
